package com.baidu.iit.broker;

import java.io.Serializable;
import java.util.List;

/**
 * @author imotai
 * 消息载体，提供只读接口，实现系列化接口，使消息能够在网络中传输
 * @param <T>
 */
public interface Message<T> extends Serializable {

	
	/**
	 * 获得消息实体
	 * @return
	 */
	T getBody();
	
	/**
	 * 获得消息头信息，消息头信息包含一些消息描述信息
	 * 比如，消息源头，消息接者
	 * @return
	 */
	List<MessageHeader> getHeaders();
	MessageHeader getHeader(String key);
	String getHandlerIdentifier();
}
