package demo;

import com.baidu.iit.broker.HandlerHasExistedException;
import com.baidu.iit.broker.Message;
import com.baidu.iit.broker.MessageBroker;
import com.baidu.iit.broker.MessageHandler;
import com.baidu.iit.broker.NoHandlerException;
import com.baidu.iit.broker.TimeoutException;

public class Main {

	public static void main(String[] args) throws NoHandlerException {
		MessageBroker broker = MessageBroker.getInstance();
		
		
		try {
			//注册hi消息处理业务
			MessageHandler hiHandler = new HIMessageHanlder();
			broker.register(hiHandler);
			
			//注册邮件消息处理业务
			MessageHandler emailHandler = new EmailMessageHandler();
			broker.register(emailHandler);
		} catch (HandlerHasExistedException e) {
			e.printStackTrace();
		}
		
		//客户端代码调用broker
		
		//发一封邮件
		EmailMessageBody emailBody = new EmailMessageBody();
		emailBody.setTitle("a little care");
		emailBody.setContent("how is eveything going?");
		emailBody.setSender("wangtaize@baidu.com");
		emailBody.setReceiver("xxxx@xxxx");
		Message emailMessage = new EmailMessage(emailBody,"email-handler");
		try {
			broker.publish(emailMessage);
		} catch (TimeoutException e) {
			e.printStackTrace();
		} catch (NoHandlerException e) {
			e.printStackTrace();
		}
		//发hi 消息
		HIMessageBody hibody= new HIMessageBody();
		hibody.setContent("how is eveything going?");
		Message hiMessage = new HIMessage(hibody,"hi-handler");
		try {
			broker.publish(hiMessage);
		} catch (TimeoutException e) {
			e.printStackTrace();
		} catch (NoHandlerException e) {
			e.printStackTrace();
		}
		System.out.println("++++++monitor+++++++++");
		System.out.println("handler count["+broker.getHandlerCount()+"]");
		System.out.println("pending message count["+broker.getAllPendingMessageSize()+"]");
		System.out.println("handler[hi-handler] pending message count["+broker.getHandlerPendingMessageSize("hi-handler")+"]");
		System.out.println("handler[email-handler] pending message count["+broker.getHandlerPendingMessageSize("email-handler")+"]");
	}

}
