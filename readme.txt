
后端业务（发邮件或者发hi消息）在MessageBroker注册一个处理逻辑，MessageBroker为每个处理逻辑
分配一个BlockQueueHandlerManager:
    这个类用队列缓存处理任务
    用线程池控制对后端服务调用并发控制

对于客户端代码，只需要Broker接口,提交需要处理的Message


BrokerManager 主要提供服务监控方法，
    1、获得BlockQueueHandlerManager数
    2、所有Pending的消息大小
    3、获得单个Handler Pending住的消息
HandlerManager 主要用于配置单个处理逻辑
    1、配置客户端代码提交任务超时时间
    2、队列大小
    3、线程池大小
          ++++++++++     +++++++++++++++++        ++++++++++++++++++
          +        +     +               +        +                +
          | Broker |     | BrokerManager |        | HandlerManager |
          +        +     +               +        +                +
          ++++++++++     +++++++++++++++++        ++++++++++++++++++
              ^  ^              ^                        ^
              :  :              :                        :
              ---:---------------                        :
                 :----:----------------------------------:
              +++++++++++++++++++       ++++++++++++++++++++++++++++
              +                 +1    n +                          +
              |  MessageBroker  |－－－> | BlockQueueHandlerManager ｜ 
              +                 +       +                          +
              +++++++++++++++++++       ++++++++++++++++++++++++++++  
                                                      | 1
                                                      |
                                                      |
                                                      V 1
                                            +++++++++++++++++++
                                            +                 +
                                            | MessagerHanlder |
                                            +                 +
                                            +++++++++++++++++++
                                            
                                            




Demo 运行
./src/test/java/demo/Main.java
