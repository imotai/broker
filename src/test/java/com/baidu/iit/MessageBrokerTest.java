package com.baidu.iit;

import com.baidu.iit.broker.HandlerHasExistedException;
import com.baidu.iit.broker.Message;
import com.baidu.iit.broker.MessageBroker;
import com.baidu.iit.broker.MessageHandler;
import com.baidu.iit.broker.NoHandlerException;
import com.baidu.iit.broker.TimeoutException;
import junit.framework.TestCase;

public class MessageBrokerTest extends TestCase{

	public MessageBrokerTest( String testName ){
        super( testName );
    }
	
	public void testThrowNoHandlerException(){
		MessageBroker broker = MessageBroker.getInstance();
		HIMessageBody hibody= new HIMessageBody();
		hibody.setContent("how is eveything going?");
		Message hiMessage = new HIMessage(hibody,"hi-handler");
		try {
			broker.publish(hiMessage);
			assertTrue( false);
		} catch (TimeoutException e) {
			assertTrue( false );
			
		} catch (NoHandlerException e) {
			// TODO Auto-generated catch block
			assertTrue( true );
		}
	}
	public void testDoNotThrowNoHandlerException(){
		MessageBroker broker = MessageBroker.getInstance();
		try {
			//注册hi消息处理业务
			MessageHandler hiHandler = new HIMessageHanlder();
			broker.register(hiHandler);
			
		} catch (HandlerHasExistedException e) {
			e.printStackTrace();
		}
		HIMessageBody hibody= new HIMessageBody();
		hibody.setContent("how is eveything going?");
		Message hiMessage = new HIMessage(hibody,"hi-handler");
		try {
			broker.publish(hiMessage);
			assertTrue( true);
		} catch (TimeoutException e) {
			assertTrue( false );
		} catch (NoHandlerException e) {
			// TODO Auto-generated catch block
			assertTrue( false );
		}
	}
	
	
}
