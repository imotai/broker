package com.baidu.iit.broker;


/**
 * @author imotai
 * 主要提供一些监控方法
 */
public interface BrokerManager {

	
	int getHandlerCount();
	int getAllPendingMessageSize();
	int getHandlerPendingMessageSize(String handlerIdentifier) throws NoHandlerException;
	
}
