package com.baidu.iit.broker;

import java.util.concurrent.BlockingQueue;

public class Task implements Runnable {

	@SuppressWarnings("rawtypes")
	private BlockingQueue<Message> messageQueue;
	private MessageHandler handler;
	private boolean running = false;
	public Task(@SuppressWarnings("rawtypes") BlockingQueue<Message> messageQueue,MessageHandler handler){
		this.messageQueue = messageQueue;
		this.handler = handler;
		this.running = true;
	}
	@Override
	public void run() {
		while(this.running){
			try {
				@SuppressWarnings("rawtypes")
				Message message = this.messageQueue.take();
				this.handler.handle(message);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
		}
	}

}
