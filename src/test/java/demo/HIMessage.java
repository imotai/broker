package demo;

import java.util.List;

import com.baidu.iit.broker.Message;
import com.baidu.iit.broker.MessageHeader;

public class HIMessage implements Message<HIMessageBody> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -856734882974030193L;

	private HIMessageBody body ;
	private String identifier;
	private List<MessageHeader> headerList;
	
	public HIMessage(HIMessageBody body, String identifier) {
		
		this.body = body;
		this.identifier = identifier;
	}

	public HIMessage(HIMessageBody body, String identifier,
			List<MessageHeader> headerList) {
		
		this.body = body;
		this.identifier = identifier;
		this.headerList = headerList;
	}

	@Override
	public HIMessageBody getBody() {
		
		return this.body;
	}

	@Override
	public List<MessageHeader> getHeaders() {
		
		return this.headerList;
	}

	@Override
	public MessageHeader getHeader(String key) {
		return null;
	}

	@Override
	public String getHandlerIdentifier() {
		
		return this.identifier;
	}
	

}
