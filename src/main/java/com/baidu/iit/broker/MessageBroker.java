package com.baidu.iit.broker;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MessageBroker implements Broker,BrokerManager {

	
	private Map<String,BlockQueueHandlerManager> handlerManagerMap;
	private static MessageBroker broker ;
	private MessageBroker(){
		handlerManagerMap = new HashMap<String,BlockQueueHandlerManager>();
	}
	public static MessageBroker getInstance(){
		if(broker != null){
			return broker;
		}
		synchronized(MessageBroker.class){
			if(broker != null){
				return broker;
			}
			broker = new MessageBroker();
			return broker;
		}
	}
	@Override
	public int getHandlerCount() {
		return this.handlerManagerMap.size();
	}

	@Override
	public void publish(Message<?> message) throws TimeoutException,
			NoHandlerException {
		String identifier = message.getHandlerIdentifier();
		if (!this.handlerManagerMap.containsKey(identifier)){
			throw new NoHandlerException("handler with identifier of"+identifier+"does not exist");
		}
		Broker handlerBroker = this.handlerManagerMap.get(identifier);
		handlerBroker.publish(message);
	}
	@Override
	public synchronized void register(MessageHandler handler)
			throws HandlerHasExistedException {
		String identifier = handler.getIdentifier();
		if (this.handlerManagerMap.containsKey(identifier)){
			throw new HandlerHasExistedException("handler with identifier of"+identifier+"has existed");
		}
		BlockQueueHandlerManager handlerManger = new BlockQueueHandlerManager();
		handlerManger.setHandler(handler);
		handlerManger.initialize();
		this.handlerManagerMap.put(identifier, handlerManger);
	}
	@Override
	public int getAllPendingMessageSize() {
		Iterator<String> keys = this.handlerManagerMap.keySet().iterator();
		int count = 0;
		while(keys.hasNext()){
			HandlerManager manager = this.handlerManagerMap.get(keys.next());
			count+=manager.getPendingMesssageSize();
		}
		return count;
	}
	@Override
	public int getHandlerPendingMessageSize(String handlerIdentifier)throws NoHandlerException {
		if(!this.handlerManagerMap.containsKey(handlerIdentifier)){
			throw new NoHandlerException("handler with identifier of"+handlerIdentifier+"does not exist");
		}
		HandlerManager manager = this.handlerManagerMap.get(handlerIdentifier);
		return manager.getPendingMesssageSize();
	}
	
	

}
