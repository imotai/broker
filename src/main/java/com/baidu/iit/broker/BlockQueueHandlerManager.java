package com.baidu.iit.broker;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class BlockQueueHandlerManager implements HandlerManager,Broker {

	private MessageHandler handler ;
	@SuppressWarnings("rawtypes")
	private BlockingQueue<Message> messageQueue;
	private int messagePoolSize = 10;
	//一分钟
	private long timeout = 60000;
	private int threadSize = 5;
	private ExecutorService service;
	/**
	 * 初始化函数
	 */
	@SuppressWarnings("rawtypes")
	public void initialize(){
		if(this.threadSize<=1){
			service = Executors.newSingleThreadExecutor(new DaemonThreadFactory());
		}else{
			service = Executors.newFixedThreadPool(this.threadSize,new DaemonThreadFactory());
		}
		messageQueue = new ArrayBlockingQueue<Message>(messagePoolSize);
		for(int i = 0;i<this.threadSize;i++){
			service.submit(new Task(this.messageQueue,this.handler));
		}
	}
	@Override
	public void publish(Message<?> message) throws TimeoutException,
			NoHandlerException {
		
		try {
			boolean result = this.messageQueue.offer(message, 
					                                 this.timeout, 
					                                 TimeUnit.MILLISECONDS);
			if(!result){
				throw new TimeoutException("add message to queue timeout");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void register(MessageHandler handler)
			throws HandlerHasExistedException { 
		
	}

	@Override
	public void setMessagePoolSize(int size) {
		this.messagePoolSize = size;
	}


	@Override
	public void setThreadSize(int size) {
		this.threadSize = size;
	}

	@Override
	public void setTimeout(long time) {
		this.timeout = time;
	}

	@Override
	public int getPendingMesssageSize() {
		return this.messageQueue.size();
	}
	@Override
	public void setHandler(MessageHandler handler) {
		this.handler = handler;
	}
	
	class DaemonThreadFactory implements ThreadFactory{

		@Override
		public Thread newThread(Runnable r) {
			Thread thread = new Thread(r);
			thread.setDaemon(true);
			return thread;
		}
		
	}
	
	

}
