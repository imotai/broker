package com.baidu.iit.broker;

/**
 * @author imotai
 * 单个后端服务流量控制接口
 */
public interface HandlerManager {

	/**
	 * 设置消息池大小
	 * @param size
	 */
	void setMessagePoolSize(int size);
	
	
	
	/**设置并发线程数，控制向后端服务调用流量
	 * @param size
	 */
	void setThreadSize(int size);
	
	
	/**
	 * 设置消息发布超时时间
	 * @param time
	 */
	void setTimeout(long time);
	
	int getPendingMesssageSize();
	
	void setHandler(MessageHandler handler);
	

	
	
}
