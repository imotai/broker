package com.baidu.iit.broker;

public class HandlerHasExistedException extends Exception {

	private static final long serialVersionUID = 6671856963258740607L;
	public HandlerHasExistedException(String message){
		super(message);
	}
	@Override
	public  Throwable fillInStackTrace() {
		return this;
	}
}
