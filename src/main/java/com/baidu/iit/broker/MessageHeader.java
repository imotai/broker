package com.baidu.iit.broker;


/**
 * @author imotai
 * 消息属性，只读接口
 */
public interface MessageHeader {

	String getKey();
	String getValue();
}
