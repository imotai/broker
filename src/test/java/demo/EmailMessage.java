package demo;

import java.util.List;

import com.baidu.iit.broker.Message;
import com.baidu.iit.broker.MessageHeader;

public class EmailMessage implements Message<EmailMessageBody> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2425455500623366711L;
	private EmailMessageBody body ;
	private String identifier;
	private List<MessageHeader> headerList;
	
	public EmailMessage(EmailMessageBody body, String identifier) {
		super();
		this.body = body;
		this.identifier = identifier;
	}

	public EmailMessage(EmailMessageBody body, String identifier,
			List<MessageHeader> headerList) {
		super();
		this.body = body;
		this.identifier = identifier;
		this.headerList = headerList;
	}

	@Override
	public EmailMessageBody getBody() {
		
		return this.body;
	}

	@Override
	public List<MessageHeader> getHeaders() {
		
		return this.headerList;
	}

	@Override
	public MessageHeader getHeader(String key) {
		
		return null;
	}

	@Override
	public String getHandlerIdentifier() {
		
		return this.identifier;
	}

}
