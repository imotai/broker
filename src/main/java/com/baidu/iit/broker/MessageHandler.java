package com.baidu.iit.broker;

public interface MessageHandler {

	
	/**
	 * 用于标识handler,必须唯一
	 * @return
	 */
	String getIdentifier();
	
	void handle(Message<?> message) throws TimeoutException;
}
