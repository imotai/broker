package com.baidu.iit.broker;

/**
 * @author imotai 对外消息代理者接口，用于控制发送消息流量
 */
public interface Broker {

	/**
	 * 给消息发布者提供的方法 在后端消息池满时，阻塞一段时间会抛出异常 
	 * 如果发送的消息，没有处理逻辑，会抛出异常
	 * 
	 * @param message
	 * @throws TimeoutException
	 * @throws NoHandlerException
	 */
	void publish(Message<?> message) throws TimeoutException,
			NoHandlerException;

	/**
	 * 注册，消息处理逻辑
	 * 
	 * @param handler
	 * @throws HandlerHasExistedException
	 */
	void register(MessageHandler handler) throws HandlerHasExistedException;
}
