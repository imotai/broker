package demo;

import com.baidu.iit.broker.Message;
import com.baidu.iit.broker.MessageHandler;
import com.baidu.iit.broker.TimeoutException;

public class EmailMessageHandler implements MessageHandler {

	@Override
	public String getIdentifier() {
		
		return "email-handler";
	}

	@Override
	public void handle(Message<?> message) throws TimeoutException {
		EmailMessageBody body = (EmailMessageBody)message.getBody();
		System.out.println("current thread is "+Thread.currentThread().getName()+" send a mail with content "+body.getContent());
	}

}
