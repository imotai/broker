package com.baidu.iit;

import com.baidu.iit.broker.Message;
import com.baidu.iit.broker.MessageHandler;
import com.baidu.iit.broker.TimeoutException;

public class HIMessageHanlder implements MessageHandler {

	@Override
	public String getIdentifier() {
		
		return "hi-handler";
	}

	@Override
	public void handle(Message<?> message) throws TimeoutException {
		HIMessageBody body = (HIMessageBody)message.getBody();
		System.out.println("current thread is "+Thread.currentThread().getName()+" send a hi message with content "+body.getContent());

	}

}
